backend-app-course-search
=========================

This is a Django app to provide course indexing and a web api for course search.
It provides support for extra custom metadata and filtering or searching on that metadata.


Development
-----------

Install in a local Open edX devstack.
Clone this repository to path/to/devstack/../src/backend_app_course_search.

Then install in the lms environment and run migrations:
::

    make lms-shell
    pip install -e /edx/src/backend_app_course_search
    exit
    make dev.migrate.lms

Also install in the studio environment.  This is to support receiving signals sent when editing a course from studio.
::

    make studio-shell
    pip install -e /edx/src/backend_app_course_search


Finally, configure elasticsearch and the initial index with ``make lms-shell`` and run the init management command: ``./manage.py lms course_search_init``.

---

Please run ``make fmt`` before committing.
This could also be called in a git hook.
This is to ensure a consistent style throughout the code, and is enforced in CI.


Usage
-----

#. Install in the LMS environment.  This step will depend on how your Open edX instance is deployed.

#. Configure ``COURSE_SEARCH_ELASTICSEARCH_CONFIG`` in the settings - see example in `backend_app_course_search/settings/devstack.py <backend_app_course_search/settings/devstack.py>`_.

#. Run LMS migrations to set up the tables used by this app.

#. Set up the models and Elasticsearch index with the management command: ``./manage.py lms course_search_init``.  This is a once off command, but it is idempotent.  Warning: deletes and recreates the ES index, so will cause a short downtime.

Note that courses are automatically indexed when changes are made, so the management command is not required for ongoing use.


License
-------

The code in this repository is licensed under the AGPL 3.0 unless
otherwise noted.

Please see `LICENSE.txt <LICENSE.txt>` for details.
