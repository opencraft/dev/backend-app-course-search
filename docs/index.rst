.. backend-app-course-search documentation top level file, created by
   sphinx-quickstart on Fri Feb 05 10:38:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

backend-app-course-search
=========================

What is this project?

Contents:

.. toctree::
   :maxdepth: 2

   readme
   getting_started
   testing
   internationalization
   modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
