"""
Django application settings - used for running on devstack.
"""


# This function is called by the edx-platform to inject custom settings into the main settings.
def plugin_settings(settings):
    """
    Specifies django environment settings
    """
    settings.COURSE_SEARCH_ELASTICSEARCH_CONFIG = {
        "hosts": "edx.devstack.elasticsearch",
        "port": "9200",
        "ssl": False,
    }
