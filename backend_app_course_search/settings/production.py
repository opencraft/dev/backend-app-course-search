"""
Django application settings for production.
"""


# This function is called by the edx-platform to inject custom settings into the main settings.
def plugin_settings(settings):
    """
    Specifies django environment settings
    """
    settings.COURSE_SEARCH_ELASTICSEARCH_CONFIG = settings.ENV_TOKENS.get(
        "COURSE_SEARCH_ELASTICSEARCH_CONFIG"
    )
