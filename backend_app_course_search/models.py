"""
Database models for backend_app_course_search.
"""
import logging
from typing import Optional

from django.db import models
from django.db.models.signals import m2m_changed, post_delete, post_save
from django.dispatch import receiver
from elasticsearch_dsl import (  # pylint: disable=no-name-in-module
    Boolean,
    Date,
    DocType,
    MetaField,
    String,
)
from model_utils.models import TimeStampedModel
from openedx.core.djangoapps.content.course_overviews.models import (  # pylint: disable=import-error
    CourseOverview,
)

log = logging.getLogger(__name__)


class Role(models.Model):
    """
    Model to store roles to choose from.
    """

    name = models.CharField(max_length=128)

    def __str__(self):
        """
        Return the string representation of this model.

        Needs to be human readable because this is used in the select dropdown.
        """
        return self.name


class Skill(models.Model):
    """
    Model to store skill levels to choose from.
    """

    name = models.CharField(max_length=128)

    def __str__(self):
        """
        Return the string representation of this model.

        Needs to be human readable because this is used in the select dropdown.
        """
        return self.name


class ContentType(models.Model):
    """
    Model to store content types to choose from.
    """

    name = models.CharField(max_length=128)

    def __str__(self):
        """
        Return the string representation of this model.

        Needs to be human readable because this is used in the select dropdown.
        """
        return self.name


class Tag(models.Model):
    """
    Model to store tags to choose from.
    """

    name = models.CharField(max_length=128)

    def __str__(self):
        """
        Return the string representation of this model.

        Needs to be human readable because this is used in the select dropdown.
        """
        return self.name


class CourseMetadata(TimeStampedModel):
    """
    Model to store custom metadata linked to a course.
    """

    course_overview = models.OneToOneField(
        CourseOverview, unique=True, related_name="+", on_delete=models.CASCADE
    )

    # metadata
    roles = models.ManyToManyField(
        Role,
        blank=True,
        related_name="courses",
    )
    skills = models.ManyToManyField(
        Skill,
        blank=True,
        related_name="courses",
    )
    content_type = models.ForeignKey(
        ContentType,
        blank=True,
        null=True,
        related_name="courses",
        on_delete=models.PROTECT,
    )
    tags = models.ManyToManyField(Tag, blank=True, related_name="courses")
    publication_date = models.DateTimeField(
        help_text=(
            "Publication date defaults to the course start date as set in Studio. "
            "If you wish to override that date, set a date on this field."
        ),
        blank=True,
        null=True,
    )

    def __str__(self):
        """
        Get a string representation of this model instance.
        """
        return str(self.course_overview_id)

    def reindex(self):
        """
        Reindex the metadata.

        We simply create and save a new document;
        the metadata id will be the same as the original if existing so it will be overwritten.
        If the course is not supposed to be visible, ensure it is not present in the index.

        Will raise errors from the elasticsearch library if there's a problem.
        """
        if self.is_visible():
            log.info("Reindexing %s", self.course_overview_id)
            doc = CourseDocument.create_from(self)
            doc.save()
        else:
            log.info(
                "Removing %s from index because not visible", self.course_overview_id
            )
            doc = CourseDocument.get(id=self.id, ignore=404)
            if doc:
                doc.delete()

    def current_es_data(self):
        """
        Return the data currently stored in ES for this instance.
        """
        doc = CourseDocument.get(id=self.id, ignore=404)
        if doc:
            return doc.to_dict()
        elif not self.is_visible():
            return "Excluded from index because course is excluded from catalog."
        else:
            return "Not indexed yet."

    def is_visible(self) -> bool:
        """
        Return True if the course should be visible in the search results.
        """
        return self.course_overview.catalog_visibility == "both"

    def course_name(self):
        """
        Return the public display name of the linked course overview.

        Designed to be used in django admin to display the name for readability.
        """
        return self.course_overview.display_name

    @classmethod
    def update_and_index_from(
        cls, course_overview: CourseOverview
    ) -> Optional["CourseMetadata"]:
        """
        Take a course_overview and build or retrieve a related CourseMetadata.

        Idempotent, and always reindexes.

        Return `None` on exception, or the CourseMetadata.
        """
        course_meta, created = cls.objects.get_or_create(
            course_overview=course_overview,
        )

        if not created:
            # We still want to save and trigger a reindex,
            # even if nothing is changed on the course metadata.
            # This ensures that the index will be completely up to date
            # with any changes from the linked CourseOverview.
            course_meta.save()


class CourseDocument(DocType):
    """
    Definition of the elasticsearch schema for the course metadata.

    Stores custom metadata from CourseMetadata,
    and metadata extracted from the linked CourseOverview.
    """

    # metadata pulled from CourseOverview
    name = String()
    short_description = String()
    key = String(fields={"raw": String(index="not_analyzed")})

    # custom metadata
    roles = String(fields={"raw": String(index="not_analyzed")}, multi=True)
    skills = String(fields={"raw": String(index="not_analyzed")}, multi=True)
    content_type = String(fields={"raw": String(index="not_analyzed")})
    tags = String(fields={"raw": String(index="not_analyzed")}, multi=True)
    publication_date = Date()
    enrollment_start = Date()
    enrollment_end = Date()
    invitation_only = Boolean()

    class Meta:
        """
        Configure meta information on the index.
        """

        dynamic = MetaField("strict")
        index = "backend_app_course_search_courses"

    @classmethod
    def create_from(cls, course_meta: CourseMetadata) -> "CourseDocument":
        """
        Create and return an instance of this document from a CourseMetadata.
        """
        # TODO: we also need field(s) to allow fuzzy searching across other attributes (tags, role, skill, etc.).
        # either *_text versions of the fields, or other_metadata_text field with concatted content.
        data = {
            "name": course_meta.course_overview.display_name or "",
            "short_description": course_meta.course_overview.short_description or "",
            "key": str(course_meta.course_overview_id),
            "roles": [role.name for role in course_meta.roles.all()],
            "skills": [skill.name for skill in course_meta.skills.all()],
            "content_type": course_meta.content_type.name
            if course_meta.content_type
            else "",
            "tags": [tag.name for tag in course_meta.tags.all()],
            "publication_date": course_meta.publication_date
            or course_meta.course_overview.start_date,
            "enrollment_start": course_meta.course_overview.enrollment_start,
            "enrollment_end": course_meta.course_overview.enrollment_end,
            "invitation_only": course_meta.course_overview.invitation_only,
            "meta": {
                "id": course_meta.id,
            },
        }

        return cls(**data)


@receiver(post_save, sender=CourseMetadata)
def course_metadata_trigger_reindex(
    sender, instance, created, raw, **kwargs
):  # pylint: disable=unused-argument
    """
    Create and/or reindex the CourseMetadata when it is changed.
    """
    try:
        instance.reindex()
    except Exception:  # pylint: disable=broad-except
        log.exception("Reindexing failed for %s", instance.course_overview_id)


@receiver(m2m_changed)
def m2m_trigger_reindex(
    sender, instance, action, reverse, **kwargs
):  # pylint: disable=unused-argument
    """
    Create and/or reindex the CourseMetadata when any m2m fields are changed.

    An extra function is required, because post_save on the CourseMetadata is called
    before any m2m fields are saved.
    This is a bit inefficient,
    because if tags and other metadata are changed in django admin for a CourseMetadata,
    The item will be reindexed up to 3 times in the request cycle...

    There are also many combinations of arguments that this function could receive.
    Only the post_{add,remove} signals in the forward direction are supported,
    since these are the signals observed to be sent from django admin during testing adding and removing tags.
    """
    # Here we only want to listen for events for m2m fields of CourseMetadata.
    if sender not in (
        CourseMetadata.tags.through,
        CourseMetadata.roles.through,
        CourseMetadata.skills.through,
    ):
        return

    if action in ("post_add", "post_remove") and not reverse:
        try:
            instance.reindex()
        except Exception:  # pylint: disable=broad-except
            log.exception("Reindexing failed for %s", instance.course_overview_id)


@receiver(post_save, sender=CourseOverview)
def trigger_reindex(
    sender, instance, created, raw, **kwargs
):  # pylint: disable=unused-argument
    """
    Create and/or reindex the CourseMetadata when a course is updated on the platform.
    """
    CourseMetadata.update_and_index_from(instance)


@receiver(post_delete, sender=CourseMetadata)
def trigger_delete_from_index(
    sender, instance, **kwargs
):  # pylint: disable=unused-argument
    """
    Delete the entry from Elasticsearch when deleted from the database.
    """
    try:
        doc = CourseDocument.get(id=instance.id, ignore=404)
        if doc:
            doc.delete()
    except Exception:  # pylint: disable=broad-except
        log.exception("Removing from index failed for %s", instance.course_overview_id)
