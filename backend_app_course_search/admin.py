"""
Django admin definitions for this app.
"""
from django.contrib import admin

from backend_app_course_search.models import (
    ContentType,
    CourseMetadata,
    Role,
    Skill,
    Tag,
)


class MetadataAdmin(admin.ModelAdmin):
    """
    Admin definition for the CourseMetadata model.

    Allows admins to edit custom metadata for courses.
    """

    readonly_fields = ("current_es_data", "course_overview")
    list_display = ("course_overview_id", "course_name")
    search_fields = ("course_overview__id", "course_overview__display_name")

    def has_delete_permissions(self, request, _obj=None):
        """
        We don't want to allow users to delete objects from here.

        The models are synced with CourseOverviews.
        """
        return False

    def has_add_permission(self, request):
        """
        We don't want to allow users to add objects from here.

        The models are auto created by the init commands and signals.
        """
        return False


class ChoiceMetadataAdmin(admin.ModelAdmin):
    """
    Django admin definition for all custom metadata models.

    These are models that just have a 'name' field,
    and shouldn't be edited because this causes pain for indexing.
    """

    def get_readonly_fields(self, request, obj=None):
        """
        'name' should not be editable after initial creation.

        Otherwise we need to add hooks and reindex all courses with this field...
        """
        if obj is not None:
            return ("name",)
        return ()


admin.site.register(CourseMetadata, MetadataAdmin)
admin.site.register(Role, ChoiceMetadataAdmin)
admin.site.register(Skill, ChoiceMetadataAdmin)
admin.site.register(ContentType, ChoiceMetadataAdmin)
admin.site.register(Tag, ChoiceMetadataAdmin)
