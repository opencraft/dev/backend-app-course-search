"""
Views for the http search api.
"""
from django.http import JsonResponse
from django.views import View

from .models import CourseDocument

DEFAULT_SORT_ORDER = "relevance"
SORT_ORDERS = {
    "relevance": [
        {"_score": {"order": "desc"}},
        {"publication_date": {"order": "desc"}},
    ],
    "pub-date": {"publication_date": {"order": "desc"}},
}
PUB_DATE_RANGES = {
    "Past month": "now-1M",
    "Past six months": "now-6M",
    "Past year": "now-1y",
}

# number of results per page
PAGINATION_SIZE = 10


def filter_empty(xs: list) -> list:
    """
    Return a new list containing only the truthy values from the `xs` list.
    """
    return [x for x in xs if x]


class SearchView(View):
    """
    View for search requests.
    """

    def get(self, request):
        """
        Perform a search against the ES index and return the processed results.

        TODO: docs for api params and example response.

        GET params:
            - q: search string - this fuzzy matches against all the fields
            - content_type[]: list of strings
            - pub_date: past-week | past-month | past-year
            - role[]: list of strings
            - skill[]: list of strings
            - tag[]: list of strings
            - sort: sort order: relevance (default) | pub-date
        """
        search_params = {}

        # the general query
        q = request.GET.get("q", "")

        # filters
        content_types = filter_empty(request.GET.getlist("content_type[]"))
        pub_date = request.GET.get("pub_date", "")
        roles = filter_empty(request.GET.getlist("role[]"))
        skills = filter_empty(request.GET.getlist("skill[]"))
        tags = filter_empty(request.GET.getlist("tag[]"))

        page_param = request.GET.get("page")
        try:
            page = int(page_param if page_param else 1)
            if page < 1:
                raise ValueError()
        except ValueError:
            return JsonResponse(
                {
                    "error": "{} is not a valid page number.".format(page_param),
                },
                status=400,
            )

        sort_str = request.GET.get("sort", DEFAULT_SORT_ORDER)
        sort_params = SORT_ORDERS.get(sort_str, SORT_ORDERS[DEFAULT_SORT_ORDER])

        if q:
            query = {
                "should": [
                    {
                        "multi_match": {
                            "fuzziness": "AUTO",
                            "fields": [
                                "name^4",
                                "short_description^3",
                                "roles",
                                "skills",
                                "content_type",
                                "tags",
                                "key",
                            ],
                            "query": q,
                        },
                    },
                ],
            }
        else:
            query = {}

        filters = {}
        if roles:
            # NOTE: '.raw' prefix is added to ensure that the strings aren't analysed/tokenised,
            # because we want exact matches only (they are chosen from available values).
            # This may not be necessary when we upgrade to a newer elasticsearch version.
            filters["roles"] = {"terms": {"roles.raw": roles}}
        if skills:
            filters["skills"] = {"terms": {"skills.raw": skills}}
        if content_types:
            filters["content_type"] = {"terms": {"content_type.raw": content_types}}
        if tags:
            filters["tags"] = {"terms": {"tags.raw": tags}}
        if pub_date in PUB_DATE_RANGES:
            filters["publication_date"] = {
                "range": {"publication_date": {"gte": PUB_DATE_RANGES[pub_date]}}
            }

        post_filter = None
        if filters:
            post_filter = {
                "bool": {
                    "must": list(filters.values()),
                }
            }

        aggregations = {
            "content_type": {"terms": {"field": "content_type.raw"}},
            "roles": {"terms": {"field": "roles.raw"}},
            "skills": {"terms": {"field": "skills.raw"}},
            "publication_date": {
                "range": {
                    "field": "publication_date",
                    "ranges": [
                        {"from": v, "to": "now", "key": k}
                        for k, v in PUB_DATE_RANGES.items()
                    ],
                }
            },
            "tags": {"terms": {"field": "tags.raw"}},
        }

        for filter_name in aggregations:
            filter_clauses = [
                value for key, value in filters.items() if key != filter_name
            ]
            aggregations[filter_name] = {
                "filter": {"bool": {"must": filter_clauses}},
                "aggs": {
                    "filtered_results": aggregations[filter_name],
                },
            }

        search_params = {
            "aggs": aggregations,
            "query": {
                "bool": query,
            },
            "sort": sort_params,
            "from": PAGINATION_SIZE * (page - 1),
            "size": PAGINATION_SIZE,
        }
        if post_filter:
            search_params["post_filter"] = post_filter

        search = CourseDocument.search()
        search.update_from_dict(search_params)
        search_response = search.execute().to_dict()

        aggregations = {
            key: [
                x for x in value.get("filtered_results", value)["buckets"] if x["key"]
            ]  # filter out empty items
            for key, value in search_response["aggregations"].items()
        }
        response_data = {
            "results": [x["_source"] for x in search_response["hits"]["hits"]],
            "aggregations": aggregations,
            "size": PAGINATION_SIZE,
            "total": search_response["hits"]["total"],
        }

        return JsonResponse(response_data)
