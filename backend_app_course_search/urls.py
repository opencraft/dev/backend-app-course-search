"""
URLs for backend_app_course_search.
"""
from django.conf.urls import url

from .views import SearchView

urlpatterns = [
    url(r"^course-search", SearchView.as_view()),
]
