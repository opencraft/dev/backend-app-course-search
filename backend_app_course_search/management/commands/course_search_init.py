from django.core.management.base import BaseCommand
from elasticsearch_dsl import Index
from openedx.core.djangoapps.content.course_overviews.models import CourseOverview

from backend_app_course_search.models import CourseDocument, CourseMetadata


class Command(BaseCommand):
    help = "Init and index everything. Idempotent."

    def handle(self, *args, **options):
        index = Index(CourseDocument._doc_type.index)
        index.delete(ignore=404)
        index.doc_type(CourseDocument)
        index.create()

        for course in CourseOverview.objects.all():
            CourseMetadata.update_and_index_from(course)
