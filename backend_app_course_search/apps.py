"""
backend_app_course_search Django application initialization.
"""

from django.apps import AppConfig


class BackendAppCourseSearchConfig(AppConfig):
    """
    Configuration for the backend_app_course_search Django application.
    """

    name = "backend_app_course_search"
    verbose_name = "Course Search"
    plugin_app = {
        "url_config": {
            "lms.djangoapp": {
                "namespace": "backend_app_course_search",
            },
        },
        "settings_config": {
            "lms.djangoapp": {
                "production": {
                    "relative_path": "settings.production",
                },
                "common": {
                    "relative_path": "settings.common",
                },
                "devstack": {
                    "relative_path": "settings.devstack",
                },
            },
            "cms.djangoapp": {
                "production": {
                    "relative_path": "settings.production",
                },
                "common": {
                    "relative_path": "settings.common",
                },
                "devstack": {
                    "relative_path": "settings.devstack",
                },
            },
        },
    }

    def ready(self):
        """
        Run extra setup tasks when this app is all set up.
        """
        from django.conf import settings
        from elasticsearch_dsl.connections import connections

        connections.configure(default=settings.COURSE_SEARCH_ELASTICSEARCH_CONFIG)
